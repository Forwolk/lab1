package ru.nailakhatov.chisla.lab1;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 * @author Nail Akhatov
 */
public class Lab1 {

    /** Таблица соответствия аргумент - значение */
    private static Map<Double, Double> table = new LinkedHashMap<>();
    /** Массив используемых аргументов */
    private static double[] arrayX;
    /** Массив используемых значений */
    private static double[] arrayY;
    /** Начальная точка */
    private static final int a = 0;
    /** Конечная точка */
    private static final int b = 10;
    /** Количество точек деления */
    private static final int N = 10;

    /**
     * Точка входа в программу
     * @param args консольные параметры (не используются, но обязаны быть в точке входа)
     */
    public static void main(String[] args) {
        arrayX = new double [N+1];
        arrayY = new double [N+1];
        for (int i = a; i <= N; i ++) { 
            double temp = (a + 1d* i * (b - a) / N);
            arrayX [i] = temp;
            arrayY [i] = f(temp);
            temp += 1d * (b - a) / (2 * N ); // +e
            table.put(temp, f(temp));
        }

        Graph graph = new Graph(arrayX, arrayY);
        
        print ();
        
        table.clear();
        
        System.out.println();
        
        for (int i = a; i <= N; i ++) { 
            double temp = (a + 1d* i * b / N);
            temp += 1d * (b - a) / (2 * N ); // +e
            arrayX [i] = temp;
            arrayY [i] = f(temp);
            table.put(temp, L(temp));
        }

        graph.addGraph(arrayX, arrayY);

        print();
    }

    /**
     * Заданная в условии функция
     * @param x аргумент
     * @return значение
     */
    private static double f (double x) {
        return Math.sin(Math.exp( x / 2 ) / 35);
    }

    /**
     * Базисный полином
     * @param n степень полинома
     * @param x аргумент
     * @return значение
     */
    private static double l (int n, double x) {
        double ans = 1;
        for (int k = 0; k <= N; k++){
            if (k != n)
                ans *= (x - arrayX [k]) / (arrayX[n] - arrayX [k]);
        }

        return ans; 
    }

    /**
     * Подсчет интерполяционного многочлена Лагранжа
     * @param x аргумент
     * @return зачение
     */
    private static double L (double x) {
        double ans = 0;
        for (int i = 0; i <= N; i ++) {
            ans += f(arrayX[i]) * l(i, x);
        }
        return ans;
    }
    
    /**
     * Печать объекта table в окно вывода
     */
    private static void print () {
        for (double t : table.keySet()) {
            String line = String.format("%s; %s", t, table.get(t));
            line = fix(line);
            System.out.println(line);
        }
    }

    /**
     * Excel не поддерживает числа через точку, поэтому меняем точки на запятые
     * @param line строка вывода
     * @return пофикшенная строка вывода
     */
    private static String fix (String line){
        return line.replace(".", ",");
    }
}
