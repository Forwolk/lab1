package ru.nailakhatov.chisla.lab1;

import javax.swing.*;
import java.awt.*;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/**
 * График функции
 */
class Graph extends JFrame{
    /** Массив аргументов */
    private final double[] x;
    /** Массив значений функции*/
    private final double[] y;
    /** Размер графика */
    private final Dimension size = new Dimension(500, 300);
    /** Начало координат */
    private final Dimension startPoint = new Dimension(40, 250);
    /** Масштаб */
    private double scaleX;
    private double scaleY;
    /** Графика */
    private Graphics graphics;
    /** Дополнительные графики */
    private Set<Map<Double, Double>> additionalGraphs = new HashSet<>();

    /**
     * Конструктор
     * @param x значения аргументов
     * @param y значения функции
     */
    Graph (double[] x, double[] y) {
        this.x = x.clone();
        this.y = y.clone();
        if (x.length != y.length)
            throw new ArrayIndexOutOfBoundsException("Количество аргументов не соответсвтует количеству значений функции!");
        calculateScale();
        reBuildArrays();
        initInterface();
    }

    /**
     * Пересчет координат графика на координаты окошка
     */
    private void reBuildArrays (){
        for (int i = 0; i < x.length; i ++){
            x[i] *= scaleX;
            y[i] *= scaleY;

            x[i] += startPoint.width;
            y[i] = startPoint.height - y[i];
        }
    }

    /**
     * Нахождение максимального элемента массива
     * @param array массив
     * @return максимальное значение
     */
    private double max (double [] array) {
        double max = array[0];
        for (double temp : array){
            if (temp > max)
                max = temp;
        }
        return max;
    }

    /**
     * Поиск оптимального масштаба
     */
    private void calculateScale (){
        double maxX = max(x);
        double maxY = max(y);
        double xPoints = size.getWidth() - 2 * startPoint.getWidth();
        double yPoints = size.getHeight() - 2 * (size.getHeight() - startPoint.getHeight());
        scaleX = xPoints/maxX;
        scaleY = yPoints/maxY;
    }

    /**
     * Открытие графического окна
     */
    private void initInterface() {
        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setSize(size);
        setResizable(false);
        setTitle("Graphics by Nail Akhatov");
        setVisible(true);
    }

    @Override
    public void paint(Graphics g) {
        this.graphics = g;
        g.setColor(Color.BLACK);
        g.fillRect(0,0,size.width,size.height);

        g.setColor(Color.WHITE);

        final double graphWidth = size.getWidth() - 2 * startPoint.getWidth();
        final double graphHeight = size.getHeight() - 2 * (size.getHeight() - startPoint.getHeight());

        int i  = 0;
        while ((scaleX * i) <= graphWidth || (scaleY * i) <= graphHeight){
            if ((scaleX * i) <= graphWidth)
                g.drawString(
                        String.valueOf(i),
                        (int) (startPoint.width + scaleX * i),
                        startPoint.height + (startPoint.width - 25)
                );
            if ((scaleY * i) <= graphHeight)
                g.drawString(
                        String.valueOf(i),
                        (int) (startPoint.getWidth() - 25),
                        (int) (startPoint.height - scaleY * i)
                );
            i++;
        }

        g.drawLine(
                startPoint.width,
                startPoint.height,
                (int) (startPoint.width + graphWidth),
                startPoint.height
        );
        g.drawLine(
                startPoint.width,
                startPoint.height,
                startPoint.width,
                (int) (startPoint.height - graphHeight)
        );

        //Основной
        plot(x, y, Color.CYAN);

        //Дополнительные
        for (Map<Double, Double> graphMap : additionalGraphs){
            int j = 0;
            double[] arrayX = new double[graphMap.size()];
            double[] arrayY = new double[graphMap.size()];
            for (double grX : graphMap.keySet()){
                arrayX[j] = grX;
                arrayY[j] = graphMap.get(grX);
                j++;
            }
            plot(arrayX, arrayY, Color.RED);
        }
    }

    /**
     * Построить график
     * @param x массив аргументов
     * @param y массив значений
     * @param color цвет линии
     */
    private void plot (double[] x, double[] y, Color color) {
        this.graphics.setColor(color);
        this.graphics.drawPolyline(transform(x),transform(y),x.length);
    }

    /**
     * Преобразование десятичного массива в целочисленный
     * @param array массив
     * @return целочисленный массив
     */
    private int[] transform (double[] array){
        int[] newArray = new int[array.length];
        for (int i = 0; i < array.length; i ++)
            newArray [i] = (int) array[i];
        return newArray;
    }

    /**
     * Дополнительный график
     * @param x аргумент
     * @param y значение
     */
    void addGraph (double[] x, double[] y) {
        double[] tempX = x.clone();
        double[] tempY = y.clone();
        if (tempX.length != tempY.length)
            throw new ArrayIndexOutOfBoundsException("Количество аргументов не соответсвтует количеству значений функции!");
        Map<Double, Double> graphMap = new LinkedHashMap<>();
        for (int i = 0; i < tempX.length; i ++) {
            tempX[i] *= scaleX;
            tempY[i] *= scaleY;

            tempX[i] += startPoint.width;
            tempY[i] = startPoint.height - tempY[i];

            graphMap.put(tempX[i], tempY[i]);
        }
        additionalGraphs.add(graphMap);
        this.validate();
    }
}
